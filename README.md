How up project:
```text
1. Pull from repository
2. create new file .env from .env.example
3. open .env and add your database params
4. run command in console: composer update
5. after run command: php artisan migrate --seed
6. Check all requests by postman
```

#### Basic CRUDs with additional methods:
- Students
    - Get all students
    - Create new student
    - Update student
    - Delete student
- Teachers
    - Get all teachers
    - Create new teacher
    - Update teacher
    - Delete teacher
- Classes
    - Get all classes
    - Create new class
    - Update class
    - Delete class  
- Additional methods by task:
    - Get all Students on the given Class
    - Get all Сlasses on the given Student
    - Get all Students on the given Class (read as Group)
    - Get daily Classes schedule on the given Class (read as Group)

## Students
### Get all students
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/students
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
[
    {
        "id": 1,
        "first_name": "Saul",
        "last_name": "Witting",
        "age": 18,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    },
    {
        "id": 2,
        "first_name": "Arlie",
        "last_name": "Homenick",
        "age": 18,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    }
]
```

### Create new student
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/students
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"first_name": "John",
	"last_name": "Smith",
	"age": 35
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "first_name": "John",
    "last_name": "Smith",
    "age": 35,
    "updated_at": "2019-03-26 10:00:20",
    "created_at": "2019-03-26 10:00:20",
    "id": 91
}
```

### Update student
Параметры запроса:
```text
Method: PUT
URL: http://{server-site-url}/students/{student_id}
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"first_name": "John2",
	"last_name": "Smith2",
	"age": 35
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": 91,
    "first_name": "John2",
    "last_name": "Smith2",
    "age": 35,
    "created_at": "2019-03-26 10:00:20",
    "updated_at": "2019-03-26 10:01:21"
}
```

### Delete student
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/students/{student_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
[]
```

## Teachers
### Get all teachers
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/teachers
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
[
    {
        "id": 1,
        "first_name": "Eric",
        "last_name": "VonRueden",
        "job_title": "Computer Programmer",
        "age": 30,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    },
    {
        "id": 2,
        "first_name": "Amelia",
        "last_name": "Kirlin",
        "job_title": "Pipelaying Fitter",
        "age": 22,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    }
]
```

### Create new teacher
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/teachers
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"first_name": "test11",
	"last_name": "test22",
	"job_title": "qweqwe",
	"age": 56
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "first_name": "test11",
    "last_name": "test22",
    "job_title": "qweqwe",
    "age": 56,
    "updated_at": "2019-03-26 10:05:02",
    "created_at": "2019-03-26 10:05:02",
    "id": 11
}
```

### Update teacher
Параметры запроса:
```text
Method: PUT
URL: http://{server-site-url}/teachers/{teacher_id}
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"first_name": "test112222222",
	"last_name": "test22222222",
	"job_title": "lol",
	"age": 56
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": 11,
    "first_name": "test112222222",
    "last_name": "test22222222",
    "job_title": "lol",
    "age": 56,
    "created_at": "2019-03-26 10:05:02",
    "updated_at": "2019-03-26 10:08:14"
}
```

### Delete teacher
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/teachers/{teacher_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
[]
```

## Classes
### Get all classes
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/groups
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
[
    {
        "id": 1,
        "title": "class-1",
        "day": "Wednesday",
        "time": "2019-01-06 06:28:41",
        "teacher_id": 1,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    },
    {
        "id": 2,
        "title": "class-2",
        "day": "Friday",
        "time": "2019-03-06 11:22:47",
        "teacher_id": 2,
        "created_at": "2019-03-25 22:25:30",
        "updated_at": "2019-03-25 22:25:30"
    }
]
```

### Create new class
Параметры запроса:
```text
Method: POST
URL: http://{server-site-url}/groups
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"title": "new test class",
	"day": "Monday",
	"time": "2019-09-01 15:15:15",
	"teacher_id": 2
}
```

Ответ сервера должен быть следующим(`Status: 201 Created`):
```json
{
    "title": "new test class",
    "day": "Monday",
    "time": "2019-09-01 15:15:15",
    "teacher_id": 2,
    "updated_at": "2019-03-26 10:13:09",
    "created_at": "2019-03-26 10:13:09",
    "id": 11
}
```

### Update class
Параметры запроса:
```text
Method: PUT
URL: http://{server-site-url}/groups/{group_id}
Headers: {
    Content-Type: application/json
}
```
Тело запроса:
```json
{
	"title": "new test class11",
	"day": "Monday",
	"time": "2019-09-01 15:15:15",
	"teacher_id": 2
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "id": 11,
    "title": "new test class11",
    "day": "Monday",
    "time": "2019-09-01 15:15:15",
    "teacher_id": 2,
    "created_at": "2019-03-26 10:13:09",
    "updated_at": "2019-03-26 10:14:18"
}
```

### Delete class
Параметры запроса:
```text
Method: DELETE
URL: http://{server-site-url}/groups/{group_id}
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 204 No Content`):
```json
[]
```

## Additional methods by task:
### Get all Students on the given Class
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/groups/{group_id}/students
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "group": {
        "title": "class-2",
        "day": "Friday"
    },
    "students": [
        {
            "id": 52,
            "first_name": "Mariana",
            "last_name": "Okuneva",
            "age": 20
        },
        {
            "id": 7,
            "first_name": "Elmo",
            "last_name": "Cronin",
            "age": 20
        },
        {
            "id": 47,
            "first_name": "Darrin",
            "last_name": "Strosin",
            "age": 17
        },
        {
            "id": 27,
            "first_name": "Karlie",
            "last_name": "Walsh",
            "age": 17
        },
        {
            "id": 56,
            "first_name": "Aubree",
            "last_name": "Rolfson",
            "age": 22
        },
        {
            "id": 27,
            "first_name": "Karlie",
            "last_name": "Walsh",
            "age": 17
        },
        {
            "id": 63,
            "first_name": "Judge",
            "last_name": "Jenkins",
            "age": 20
        },
        {
            "id": 21,
            "first_name": "Cara",
            "last_name": "Harber",
            "age": 17
        },
        {
            "id": 89,
            "first_name": "Bell",
            "last_name": "Johnston",
            "age": 18
        },
        {
            "id": 43,
            "first_name": "Breanna",
            "last_name": "Klocko",
            "age": 19
        },
        {
            "id": 75,
            "first_name": "Katlyn",
            "last_name": "Kuphal",
            "age": 21
        },
        {
            "id": 7,
            "first_name": "Elmo",
            "last_name": "Cronin",
            "age": 20
        },
        {
            "id": 65,
            "first_name": "Carol",
            "last_name": "Kunze",
            "age": 16
        },
        {
            "id": 21,
            "first_name": "Cara",
            "last_name": "Harber",
            "age": 17
        }
    ]
}
```

### Get all Сlasses on the given Student
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/students/{student_id}/groups
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "student": {
        "id": 7,
        "first_name": "Elmo",
        "last_name": "Cronin",
        "age": 20
    },
    "groups": [
        {
            "title": "class-2",
            "day": "Friday",
            "time": "2019-03-06 11:22:47"
        },
        {
            "title": "class-8",
            "day": "Saturday",
            "time": "2019-01-02 03:28:07"
        },
        {
            "title": "class-8",
            "day": "Saturday",
            "time": "2019-01-02 03:28:07"
        },
        {
            "title": "class-3",
            "day": "Tuesday",
            "time": "2019-02-09 16:42:09"
        },
        {
            "title": "class-6",
            "day": "Monday",
            "time": "2019-01-23 07:44:11"
        },
        {
            "title": "class-2",
            "day": "Friday",
            "time": "2019-03-06 11:22:47"
        }
    ]
}
```

### Get all Students on the given Class (read as Group)
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/groups/students
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
[
    {
        "group": {
            "title": "class-1",
            "day": "Wednesday"
        },
        "students": [
            {
                "id": 82,
                "first_name": "Demond",
                "last_name": "Rodriguez",
                "age": 16
            },
            {
                "id": 31,
                "first_name": "Austyn",
                "last_name": "Ryan",
                "age": 18
            },
            {
                "id": 19,
                "first_name": "Rolando",
                "last_name": "Shields",
                "age": 22
            },
            {
                "id": 67,
                "first_name": "Elsie",
                "last_name": "Dare",
                "age": 21
            }
        ]
    },
    {
        "group": {
            "title": "class-2",
            "day": "Friday"
        },
        "students": [
            {
                "id": 52,
                "first_name": "Mariana",
                "last_name": "Okuneva",
                "age": 20
            },
            {
                "id": 7,
                "first_name": "Elmo",
                "last_name": "Cronin",
                "age": 20
            },
            {
                "id": 47,
                "first_name": "Darrin",
                "last_name": "Strosin",
                "age": 17
            }
        ]
    }
]
```

### Get daily Classes schedule on the given Class (read as Group)
Параметры запроса:
```text
Method: GET
URL: http://{server-site-url}/groups/schedule
Headers: {
    Content-Type: application/json
}
```

Ответ сервера должен быть следующим(`Status: 200 OK`):
```json
{
    "Monday": [
        {
            "title": "class-6",
            "time": "2019-01-23 07:44:11",
            "teacher": {
                "job_title": "Director Religious Activities",
                "id": 6
            }
        },
        {
            "title": "ewrwdfsfsewgsd22211",
            "time": "2019-09-01 15:15:15",
            "teacher": {
                "job_title": "Pipelaying Fitter",
                "id": 2
            }
        }
    ],
    "Tuesday": [
        {
            "title": "class-3",
            "time": "2019-02-09 16:42:09",
            "teacher": {
                "job_title": "Machine Operator",
                "id": 3
            }
        },
        {
            "title": "class-5",
            "time": "2019-03-17 17:49:57",
            "teacher": {
                "job_title": "Loading Machine Operator",
                "id": 5
            }
        }
    ],
    "Wednesday": [
        {
            "title": "class-1",
            "time": "2019-01-06 06:28:41",
            "teacher": {
                "job_title": "Computer Programmer",
                "id": 1
            }
        }
    ],
    "Thursday": [
        {
            "title": "class-4",
            "time": "2019-03-08 03:20:20",
            "teacher": {
                "job_title": "Architectural Drafter OR Civil Drafter",
                "id": 4
            }
        },
        {
            "title": "class-7",
            "time": "2019-01-04 02:42:53",
            "teacher": {
                "job_title": "Cafeteria Cook",
                "id": 7
            }
        },
        {
            "title": "class-9",
            "time": "2019-02-03 08:29:20",
            "teacher": {
                "job_title": "Purchasing Agent",
                "id": 9
            }
        }
    ],
    "Friday": [
        {
            "title": "class-2",
            "time": "2019-03-06 11:22:47",
            "teacher": {
                "job_title": "Pipelaying Fitter",
                "id": 2
            }
        }
    ],
    "Saturday": [
        {
            "title": "class-8",
            "time": "2019-01-02 03:28:07",
            "teacher": {
                "job_title": "Traffic Technician",
                "id": 8
            }
        },
        {
            "title": "class-10",
            "time": "2019-03-20 07:25:11",
            "teacher": {
                "job_title": "Excavating Machine Operator",
                "id": 10
            }
        }
    ]
}
```