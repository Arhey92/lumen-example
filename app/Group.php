<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{

    protected $table = 'classes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'day',
        'time',
        'teacher_id'
    ];

    public function teacher()
    {
        return $this->hasOne('App\Teacher', 'id', 'teacher_id');
    }

    public function students()
    {
        return $this->hasMany('App\StudentGroup', 'classes_id', 'id');
    }

    public static function getClassesByDay($workDay)
    {
        return self::with('teacher:job_title,id')->where('day', $workDay)->get()->makeHidden(['id', 'day', 'teacher_id', 'created_at', 'updated_at']);
    }
}
