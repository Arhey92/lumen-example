<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();

        return response()->json($groups, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100|string',
            'day' => 'required|string',
            'time' => 'required',
            'teacher_id' => 'required|integer|exists:teachers,id'
        ]);

        $group = Group::create($request->all());

        return response()->json($group, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'max:100|string',
            'day' => 'string',
            'teacher_id' => 'integer|exists:teachers,id'
        ]);

        $group = Group::find($id);
        if(is_null($group)){
            return response()->json("Group with id = $id not found in db", 400);
        }

        $group->update($request->all());

        return response()->json($group, 200);
    }

    public function destroy($id)
    {
        $group = Group::find($id);

        if(is_null($group)){
            return response()->json("Group with id = $id not found in db", 400);
        }

        $group->delete();

        return response()->json([], 204);
    }

    public function getStudentsByGroup($id)
    {
        $groupWithStudents = Group::with('students')->find($id);

        if(is_null($groupWithStudents)){
            return response()->json("Group with id = $id not found in db", 400);
        }

        $collection = collect();
        $collection->put('group', $groupWithStudents->only(['title', 'day']));

        $students = collect();
        foreach($groupWithStudents->students as $student){
            $students->push($student->student->only(['id', 'first_name', 'last_name', 'age']));
        }

        $collection->put('students', $students);

        return response()->json($collection, 200);
    }

    public function getStudentsByGroups()
    {
        $groupsWithStudents = Group::with('students')->get();

        $collection = collect();
        foreach($groupsWithStudents as $groupWithStudents){
            $group = collect();
            $group->put('group', $groupWithStudents->only(['title', 'day']));

            $students = collect();
            foreach($groupWithStudents->students as $student){
                $students->push($student->student->only(['id', 'first_name', 'last_name', 'age']));
            }

            $group->put('students', $students);

            $collection->push($group);
        }


        return response()->json($collection, 200);
    }

    public function getScheduleByGroups()
    {
        $workDays = app('db')->table('classes')->groupBy('day')->pluck('day');

        $results = collect();
        foreach($workDays as $workDay){
            $results->put($workDay, Group::getClassesByDay($workDay));
        }

        return $results;
    }
}
