<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();

        return response()->json($students, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'age' => 'required|integer',
//            'class_id' => 'required|integer|exists:classes,id'
        ]);

        if($request->age > 255){
            return response()->json("Age is not correct. Max 255.", 400);
        }

        $student = Student::create($request->all());

        return response()->json($student, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'string|max:50',
            'last_name' => 'string|max:50',
            'age' => 'integer',
//            'class_id' => 'integer|exists:classes,id'
        ]);

        if($request->age > 255){
            return response()->json("Age is not correct. Max 255.", 400);
        }

        $student = Student::find($id);
        if(is_null($student)){
            return response()->json("Student with id = $id not found in db", 400);
        }

        $student->update($request->all());

        return response()->json($student, 200);
    }

    public function destroy($id)
    {
        $student = Student::find($id);

        if(is_null($student)){
            return response()->json("Teacher with id = $id not found in db", 400);
        }

        $student->delete();

        return response()->json([], 204);
    }

    public function getGroupsByStudent($id)
    {
        $studentWithGroups = Student::with('groups')->find($id);

        if(is_null($studentWithGroups)){
            return response()->json("Student with id = $id not found in db", 400);
        }

        $collection = collect();
        $collection->put('student', $studentWithGroups->only(['id', 'first_name', 'last_name', 'age']));

        $groups = collect();
        foreach($studentWithGroups->groups as $group){
            $groups->push($group->group->only(['title', 'day', 'time']));
        }

        $collection->put('groups', $groups);

        return response()->json($collection, 200);
    }
}
