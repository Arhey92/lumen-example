<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = Teacher::all();

        return response()->json($teachers, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:50|string',
            'last_name' => 'required|max:50|string',
            'job_title' => 'required|string',
            'age' => 'required|integer'
        ]);

        $teacher = Teacher::create($request->all());

        return response()->json($teacher, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'string|max:50',
            'last_name' => 'string|max:50',
            'job_title' => 'string',
            'age' => 'integer'
        ]);

        $teacher = Teacher::find($id);
        if(is_null($teacher)){
            return response()->json("Teacher with id = $id not found in db", 400);
        }

        $teacher->update($request->all());

        return response()->json($teacher, 200);
    }

    public function destroy($id)
    {
        $teacher = Teacher::with('groups')->find($id);

        if(is_null($teacher)){
            return response()->json("Teacher with id = $id not found in db", 400);
        }

        //check if exist any group with this teacher
        if($teacher->groups->count() > 0){
            return response()->json("You cant delete this teacher, because he/she has at least 1 group", 400);
        }

        $teacher->delete();

        return response()->json([], 204);
    }
}
