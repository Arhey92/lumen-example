<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'age',
        'class_id'
    ];

    public function group()
    {
        return $this->hasOne('App\Group', 'id', 'class_id');
    }

    public function groups()
    {
        return $this->hasMany('App\StudentGroup', 'student_id', 'id');
    }
}
