<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentGroup extends Model
{
    protected $table = 'student_classes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id',
        'classes_id'
    ];

    public $timestamps = false;
    public $incrementing = false;

    public function group()
    {
        return $this->hasOne('App\Group', 'id', 'classes_id');
    }

    public function student()
    {
        return $this->hasOne('App\Student', 'id', 'student_id');
    }
}
