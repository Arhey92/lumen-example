<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'job_title',
        'age'
    ];

    public function groups()
    {
        return $this->hasMany('App\Group', 'teacher_id', 'id');
    }
}
