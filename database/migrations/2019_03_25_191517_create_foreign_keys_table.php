<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('students', function (Blueprint $table) {
//            $table->foreign('class_id')->references('id')->on('classes');
//        });

        Schema::table('classes', function (Blueprint $table) {
            $table->foreign('teacher_id')->references('id')->on('teachers');
        });
    }

    public function down()
    {
//        Schema::table('students', function (Blueprint $table) {
//            $table->dropForeign(['class_id']);
//        });

        Schema::table('classes', function (Blueprint $table) {
            $table->dropForeign(['teacher_id']);
        });
    }
}
