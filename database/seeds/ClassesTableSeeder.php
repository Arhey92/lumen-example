<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = Faker\Factory::create();

        for($i = 1; $i < 11; $i++) {
            App\Group::create([
                'title' => 'class-'.$i,
                'day' => $faker->dayOfWeek,
                'time' => $this->rand_date('1-01-2019', \Carbon\Carbon::now()),
                'teacher_id' => $i
            ]);
        }
    }

    function rand_date($min_date, $max_date) {
        $min_epoch = strtotime($min_date);
        $max_epoch = strtotime($max_date);

        $rand_epoch = rand($min_epoch, $max_epoch);

        return date('Y-m-d H:i:s', $rand_epoch);
    }
}
