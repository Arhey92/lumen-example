<?php

use Illuminate\Database\Seeder;

class StudentClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 90; $i++) {
            App\StudentGroup::create([
                'student_id' => rand(1, 90),
                'classes_id' => rand(1, 10)
            ]);
        }
    }
}
