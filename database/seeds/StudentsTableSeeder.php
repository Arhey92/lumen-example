<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 90; $i++) {
            App\Student::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'age' => rand(16, 22),
//                'class_id' => rand(1, 10)
            ]);
        }
    }
}
