<?php

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; $i++) {
            App\Teacher::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'job_title' => $faker->jobTitle,
                'age' => rand(22, 60)
            ]);
        }
    }
}
