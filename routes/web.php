<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'teachers'], function () use ($router) {
    $router->get('/', 'TeacherController@index');
    $router->post('/', 'TeacherController@store');
    $router->put('/{id}', 'TeacherController@update');
    $router->delete('/{id}', 'TeacherController@destroy');
});

$router->group(['prefix' => 'students'], function () use ($router) {
    $router->get('/', 'StudentController@index');
    $router->post('/', 'StudentController@store');
    $router->put('/{id}', 'StudentController@update');
    $router->delete('/{id}', 'StudentController@destroy');

    $router->get('/{id}/groups', 'StudentController@getGroupsByStudent');
});

$router->group(['prefix' => 'groups'], function () use ($router) {
    $router->get('/', 'GroupController@index');
    $router->post('/', 'GroupController@store');
    $router->put('/{id}', 'GroupController@update');
    $router->delete('/{id}', 'GroupController@destroy');

    $router->get('/{id}/students', 'GroupController@getStudentsByGroup');
    $router->get('/students', 'GroupController@getStudentsByGroups');
    $router->get('/schedule', 'GroupController@getScheduleByGroups');
});